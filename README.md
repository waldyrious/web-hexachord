# Credit

Conceived and developped by Corentin Guichaoua and Moreno Andreatta

# Acknowledgments

Thanks to Louis Bigo for the original Hexachord software.

Thanks to Philipp Legner for improving on the initial visual design and his feedback.

Thanks to people who helped translate the software to other languages:
    German: Philipp Legner
    Hindi: Nilesh Trivedi

Sample MIDI tracks are interpretted by Moreno Andreatta.

Thanks to all collaborators for inspiration.

Thanks to USIAS / University of Strasbourg / IRMA / IRCAM for financial support.

# Citation
www.gitlab.com/guichaoua/web-hexachord
Academic paper to come, please check back later
